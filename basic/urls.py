from django.urls import path
from .views import pagina_principal, pagina_cms, estilos_css

urlpatterns = [
    path('', pagina_principal, name='index'),
    path('cms/', pagina_cms, name='cms'),
    path('main.css', estilos_css, name='css_style'),
]
