import random
from django.http import HttpResponse
from django.shortcuts import render

def pagina_principal(request):
    return HttpResponse("Esta es la página principal. Pregunta por /cms o /main.css")

def pagina_cms(request):
    content = "Esta es la página 'cms'."
    return render(request, "cms/cms.html", {"content": content})

def estilos_css(request):
    colors = ["blue", "gold", "red", "aqua", "green", "black", "yellow"]
    color, background = random.choices(colors, k=2)

    contenido = f"""
    body {{
        margin: 10px 20% 50px 70px;
        font-family: sans-serif;
        color: {color};
        background: {background};
    }}
    """
    return HttpResponse(contenido, content_type="text/css")
